import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsapiservicesService {

  constructor(private _http:HttpClient) { }

  //neswaapi url
  newsApiUrl = "https://newsapi.org/v2/top-headlines?country=in&apiKey=0cfb5b95ac1d4f58a2f27e523a8f3f50";

  topHeading():Observable<any>
  {
    return this._http.get(this.newsApiUrl);
  }
}
